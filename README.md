# Gulp
Gulp tasks like a pro! :100:


### Install
Load dependencies: `npm install`



### Tasks
- gulp css
	- [x] Compile sass and scss files
	- [x] Adds autoprefixer (for better compatibility)
	- [x] Concatenate all styles (less requests)
	- [x] Minify all styles (reduce file sizes)

- gulp js
	- [x] Concatenate all javascripts (less requests)
	- [x] Minify all javascripts (reduce file sizes)

- gulp img
	- [x] Optimize images with TinyPNG

- gulp
	- [x] Watch changes and compile css/js

- gulp uncss
	- [x] Makes your CSS files lighter and more readable
	- [x] Remove unused CSS selectors

- gulp live
	- [x] PHP WebServer
