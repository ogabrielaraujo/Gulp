// Gulpfile.js
	'use strict';

// configs
	var projectUrl	= 'exemplo.local.com',
		base		= 'content/themes/',
		// styles css
		sassSRC		= base + 'assets/sass',
		cssNAME		= 'style',
		cssDEST		= base + 'dist/css',
		// javascripts
		jsSRC		= base + 'assets/js',
		jsNAME		= 'script',
		jsDEST		= base + 'dist/js',
		// images
		imgSRC		= base + 'assets/img',
		imgDEST		= base + 'dist/img'

// plugins
	var gulp = require('gulp'),
		sass = require('gulp-sass'),
		concat = require('gulp-concat'),
		autoprefixer = require('gulp-autoprefixer'),
		tinypng = require('gulp-tinypng-compress'),
		watch = require('gulp-watch'),
		notify = require("gulp-notify"),
		uglifycss = require('gulp-uglifycss'),
		uglify = require('gulp-uglify'),
		shorthand = require('gulp-shorthand'),
		uncss = require('gulp-uncss'),
		connect = require('gulp-connect-php'),
		changed = require('gulp-changed'),
		bourbon = require("node-bourbon").includePaths

// CSS
	gulp.task('css', function() {
		gulp.src(['./' + sassSRC + '/*.scss', './' + sassSRC + '/*.sass'])
			// changed
			.pipe(changed('./' + cssDEST))

			// sass
			.pipe(sass({
				// nested, expanded(clean), compact(inline), compressed(min)
				outputStyle: 'expanded',
				includePaths: bourbon
			}).on('error', sass.logError))

			// autoprefixer
			.pipe(autoprefixer({
				browsers: ['last 5 versions'],
				cascade: false
			}))

			// concat (style.css)
			.pipe(concat(cssNAME + '.css', {newLine: ''}))
			.pipe(gulp.dest('./' + cssDEST))

			// concat + uglifycss (style.min.css)
			.pipe(concat(cssNAME + '.min.css', {newLine: ''}))
			.pipe(uglifycss({ 'uglyComments': false }))
			.pipe(gulp.dest('./' + cssDEST))

			// notify
			.pipe(notify({
				title: 'CSS',
				message: 'CSS gerado com sucesso! 💯',
				wait: false
			}))
	});

// JS
	gulp.task('js', function() {
		gulp.src('./' + jsSRC + '/*.js')
			// changed
			.pipe(changed('./' + jsDEST))

			// concat (script.js)
			.pipe(concat(jsNAME + '.js', {newLine: ''}))
			.pipe(gulp.dest('./' + jsDEST))

			// concat + uglify (script.min.js)
			.pipe(concat(jsNAME + '.min.js', {newLine: ''}))
			.pipe(uglify())
			.pipe(gulp.dest('./' + jsDEST))

			// notify
			.pipe(notify({
				title: 'JS',
				message: 'JS gerados com sucesso! 💯',
				wait: false
			}))
	});

// IMG
	gulp.task('img', function () {
		gulp.src('./' + imgSRC + '/*.{png,jpg,jpeg}')
			// tinypng
			.pipe(tinypng({
				key: 'X9Lb4ld6MGCzWBK7Qgovgtlrq_QQOvx1', // secret key
				log: true
			}))

			// destination
			.pipe(gulp.dest('./' + imgDEST))

			// notify
			.pipe(notify({
				title: 'IMG',
				message: 'Imagens geradas com sucesso! 💯',
				wait: false
			}))
	});

// WATCH (default)
	gulp.task('default', function() {
		// css
		gulp.watch(['./' + sassSRC + '/*.scss', './' + sassSRC + '/*.sass'], ['css'])

		// js
		gulp.watch(['./' + jsSRC + '/*.js'], ['js'])
	});

// UNCSS
	gulp.task('uncss', function() {
		gulp.src(['./' + sassSRC + '/*.scss', './' + sassSRC + '/*.sass'])
			// shorthand
			.pipe(shorthand())

			// uncss
			.pipe(uncss({
				html: ['http://' + projectUrl]
			}))

			// notify
			.pipe(notify({
				title: 'UNCSS',
				message: 'CSS gerado com sucesso! 💯',
				wait: false
			}))
	});

// WEBSERVER
	gulp.task('live', function() {
		// connect
		connect.server({
			hostname: projectUrl,
			port: 80,
			open: true
		})
	});
